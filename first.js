var currentResult = 0,
	previousResult;

function calculate(number, operation) {
	operation = operation || '+';
	previousResult = currentResult;
	switch(operation){
		case '+':
			currentResult += number;
			break;
		case '-':
			currentResult -= number;
			break;
		case '*':
			currentResult *= number;
			break;
		case '/':
			if (number !==0) {
				currentResult /= number;
			}
			break;
	}
	currentResult = Math.round(currentResult * 100) / 100;
	console.log(previousResult + ' ' + operation + ' ' + number + ' = ' + currentResult);
	return calculate;
}

calculate(10)(2, '+')(4, '/');